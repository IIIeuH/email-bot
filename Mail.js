const config        = require('./config'),
      File          = require('./File'),
      fs            = require('fs'),
      nodemailer    = require('nodemailer');


const transporter = nodemailer.createTransport(config.smpts);

class Mail {
    constructor(msg){
        this.message = msg;
    }

    async params(){
        try{
            if(this.message.hasOwnProperty('photo')){
                let file = new File(this.message.photo[0].file_id);
                await file.getFile();
                await file.downloadFile(`https://api.telegram.org/file/bot${config.telToken}/`);
                return {
                    from: config.from,
                    to: config.to,
                    subject: config.subject,
                    text: (this.message.caption || ''),
                    html: '<b>' + (this.message.caption || '') + '</b>',
                    attachments: {
                        filename: 'telegram.jpg',
                        content: fs.createReadStream('telegram.jpg')
                    }
                };
            }
            if(this.message.hasOwnProperty('text')){
                return {
                    from: config.from,
                    subject: config.subject,
                    text: this.message.text,
                    html: '<b>' + this.message.text + '</b>'
                };
            }
            if(this.message.hasOwnProperty('document')){
                let file = new File(this.message.document.thumb.file_id);
                await file.getFile();
                await file.downloadFile(`https://api.telegram.org/file/bot${config.telToken}/`);
                return {
                    from: config.from,
                    subject: config.subject,
                    text: (this.message.caption || ''),
                    html: '<b>' + (this.message.caption || '') + '</b>',
                    attachments: {
                        filename: 'telegram.jpg',
                        content: fs.createReadStream('telegram.jpg')
                    }
                };
            }
        }catch(err){
            throw (err);
        }
    }

    async sendMail(){
        try{
            config.mailList.forEach( async (mail) => {
                let options = await this.params();
                options.to = mail;
                await transporter.sendMail(options);
            });
        }catch(err){
            throw (`Ошибка отправки почты! ${err}`);
        }
    }
}

module.exports = Mail;