const Axios  = require('axios'),
      Fs     = require('fs'),
      axiosHttpsProxy = require('axios-https-proxy'),
      config = require('./config');

Axios.interceptors.request.use(axiosHttpsProxy);


class File {
    constructor(id){
        this.id = id;
    }

    async getFile(){
        try{
            let url = `https://api.telegram.org/bot${config.telToken}/getFile?file_id=${this.id}`;
            let res = await Axios({
                method: 'get',
                url: url
            });
            return res.data.result.file_path;
        }catch(err){
            console.log(err);
            throw ('Ошибка получения файла!');
        }
    }

    async downloadFile(url){
        const response = await Axios({
            method: 'GET',
            url: url + await this.getFile(),
            responseType: 'arraybuffer'
        });
        response.data.pipe(Fs.createWriteStream('telegram.jpg'));
    }
}

module.exports = File;