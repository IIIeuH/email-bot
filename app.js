process.env["NTBA_FIX_319"] = 1;

const MailListener  = require("mail-listener2"),
      TelegramBot   = require('node-telegram-bot-api'),
      cheerio       = require('cheerio'),
      config        = require('./config'),
      fs            = require('fs'),
      Mail          = require('./Mail');


const bot = new TelegramBot(config.telToken, config.botOptions);


const MAILLISTENER = new MailListener(config.mailLestener);

bot.on('message', async (msg) => {
    try{
        let mail = new Mail(msg);
        await mail.sendMail();
    }catch(err){
        bot.sendMessage(config.chatId, err);
    }
});

MAILLISTENER.start();

MAILLISTENER.on("server:connected", function(){
    console.log("imapConnected");
});
MAILLISTENER.on("error", function(err){
    console.log(err);
});
MAILLISTENER.on("mail", function(mail, seqno, attributes){
    const $ = cheerio.load(mail.html);
    // $('div.gmail_quote').remove();
    // let text = $('div[dir="auto"]').eq(0).text();
    let text = $('.MsoNormal').text();
    bot.sendMessage(config.chatId, `Новое письмо от: ${mail.from[0].address}\n\n ${text}`);
});

MAILLISTENER.on("attachment", function(attachment){
    const buffer = fs.readFileSync(`images/${attachment.fileName}`);
    bot.sendPhoto(config.chatId, buffer, {caption: "Фото в письме"});
});

module.exports.bot = bot;